# Libopencm3-plus Examples

Basic examples of Libopencm3-plus usage.

Tested devices:

* STM32F3 Discovery Kit
* STM32F4 Discovery Kit, version: STM32F407G-DISC1
* STM32F429 Discovery Kit, version: STM32F429I-DISC1
